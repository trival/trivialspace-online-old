require [
  'page/pageUtils'
  'external/jquery-plugins/jquery.hoverIntent.minified'
], (pageUtils) ->

  $ ->

    #buttons bar animation
    buttons = $ '#draft-button-container'
    area = $ '#buttons-hover-area'
    area.hoverIntent (->
      buttons.fadeIn 'slow'
    ), -> buttons.fadeOut 'slow'

    #comments button
    talkButton = $('#talk-button')
    comments = $('#draft-comments')
    talkButton.toggle ( ->
        comments.fadeIn()
        talkButton.addClass('selected')
        pageUtils.arrangeBottom()
      ) , ( ->
        comments.animate {
          height: '0px'
          marginTop: '0px'
          opacity: 0
        }, 400, ->
          comments.css('display', 'none').css('opacity', '').css('height', '').css('margin-top', '')
          pageUtils.arrangeBottom()

        talkButton.removeClass('selected')
    )

    #info button
    infoButton = $('#info-button')
    info = $('#draft-info')
    infoButton.toggle ( ->
        info.fadeIn()
        infoButton.addClass('selected')
        pageUtils.arrangeBottom()
      ) , ( ->
        info.animate {
          marginTop: '0px'
          height: '0px'
          opacity: 0
        }, 400, ->
          info.css('display', 'none').css('opacity', '').css('height', '').css('margin-top', '')
          pageUtils.arrangeBottom()

        infoButton.removeClass('selected')
    )

    #fullscreen button
    fullscreenButton = $('#fullscreen-button')
    fullscreenButton.click ->
      window.location.href = fullscreenButton.attr('data-href')
