define

  renderTarget: (width, height) ->
    new THREE.WebGLRenderTarget width, height,
      minFilter: THREE.LinearFilter
      magFilter: THREE.LinearFilter
      format: THREE.RGBFormat
