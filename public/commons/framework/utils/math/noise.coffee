define [
  "external/simplex-noise.min"
], (SimplexNoise) ->

  snoise = new SimplexNoise

  exports =

    noise4d: (x, y, z, w) ->
      snoise.noise4D x, y, z, w


    noise3d: (x, y, z) ->
      snoise.noise3D x, y, z


    noise2d: (x, y) ->
      snoise.noise2D x, y


    tileNoise: (width, height, x1, x2, y1, y2) ->
      noise = []
      for y in [0..height-1]
        for x in [0..width-1]
          s = x / width
          t = y / height
          dx = x2 - x1
          dy = y2 - y1

          nx = x1 + Math.cos(s * 2 * Math.PI) * dx / (2 * Math.PI)
          ny = y1 + Math.cos(t * 2 * Math.PI) * dy / (2 * Math.PI)
          nz = x1 + Math.sin(s * 2 * Math.PI) * dx / (2 * Math.PI)
          nw = y1 + Math.sin(t * 2 * Math.PI) * dy / (2 * Math.PI)

          noise.push snoise.noise4D nx, ny, nz, nw
      noise

