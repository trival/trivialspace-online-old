varying vec3 vNormal;
varying vec3 vViewPos;

void main(void)
{
    vec3 matColor = vec3(0.5);
    vec3 color = ambientLightColor*matColor
            + calculatePointLighting(vPointLight,vNormal,vViewPos,30.0,matColor,matColor)
            + calculateDirectionalLighting(vNormal,vViewPos,30.0,matColor,matColor);
    gl_FragColor = vec4(color,1.0);
}

