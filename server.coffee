express = require 'express'
request = require 'request'
assets = require 'connect-assets'
engines = require 'consolidate'
draftcontroller = require './server/draftcontroller'
partials = require 'express-partials'
config = require './server/server-config'

app = express()

app.set 'view engine', 'eco'
app.engine 'eco', engines.eco
partials.register ".html", "eco"
app.set 'views', 'server/views'
app.use partials()
app.use express.bodyParser()
app.use app.router
app.use assets src: config.PUBLIC_PATH, buildDir: false
app.use express.static config.PUBLIC_PATH
app.use express.errorHandler dumpExceptions: true, showStack: true

app.locals.moment = require 'moment'
app.locals.mdToHtml = require('markdown').markdown.toHTML

# proxy to couchdb
app.get /^\/db(\/.*)$/, (req, res) ->
  query = require('url').parse(req.url, true).search
  url = config.DB_URL + "/" + config.DB_DATABASE + req.params[0] + query

  console.log '------- db-query ------- \nmethod: ' + req.method
  console.log 'url: ' + url

  req.pipe(request url).pipe(res)

# needed to provide compiled coffeescript files
app.get /^(\/[^\s]+)\.js$/, (req, res, next) ->
  js req.params[0]
  next()

# needed to provide compiled less files
app.get /^(\/[^\s]+)\.css$/, (req, res, next) ->
  css req.params[0]
  next()

draftcontroller.init app

module.exports = app

app.listen config.SERVER_PORT
console.log "server started on port " + config.SERVER_PORT
