define [
  "./pointLightWidget"
  "./directionalLightWidget"

], ->

  class LightsContoller

    constructor: (@scene, @scale) ->

      dialogDiv = $("<div />").dialog
        autoOpen: false
        closeOnEscape: false
        minWidth: 350
        resizable: false
        title: 'Lights Controller'

      toggleLight = true
      $(document).keypress (evt) ->
        if "l".charCodeAt(0) is evt.charCode and not $('*:focus').is('input, textarea')
          if toggleLight
            dialogDiv.dialog 'open'
          else
            dialogDiv.dialog 'close'
          toggleLight = !toggleLight

      accDiv = $('<div />').appendTo dialogDiv

      index = 1
      for light in @scene.__lights when light instanceof THREE.PointLight
        name = light.name or ('Point Light ' + index++)
        accDiv.append "<h3><a href='#'>#{name}</a></h3>
                      <div class='pointlightwidget'></div>"
        accDiv.find('.pointlightwidget:last').pointlight
          light: light
          scene: @scene
          scale: @scale

      index = 1
      for light in @scene.__lights when light instanceof THREE.DirectionalLight
        name = light.name or ('Directional Light ' + index++)
        accDiv.append "<h3><a href='#'>#{name}</a></h3>
                       <div class='directionallightwidget'></div>"
        accDiv.find('.directionallightwidget:last').dirlight
          light: light
          scene: @scene

      accDiv.accordion
        autoHeight: false
        clearStyle: true
