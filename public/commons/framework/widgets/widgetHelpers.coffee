define
  loadWidgetCss: ->
    unless window.__isJQueryWidgetStyleLoaded__
      $("head").append "<link>"
      css = $("head").children(":last")
      css.attr
        rel:  "stylesheet"
        type: "text/css"
        href: "/commons/external/jquery-ui/jquery-ui.css"

      window.__isJQueryWidgetStyleLoaded__ = true

  loadLightWidgetCss: ->
    unless window.__isLightWidgetStyleLoaded__
      $("head").append "<link>"
      css = $("head").children(":last")
      css.attr
        rel:  "stylesheet"
        type: "text/css"
        href: "/commons/framework/widgets/css/lightwidgets.css"

      window.__isLightWidgetStyleLoaded__ = true


