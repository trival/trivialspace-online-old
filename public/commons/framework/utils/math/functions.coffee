define

  sign: (num) ->
    if num > 0
      1
    else if num < 0
      -1
    else
      0


  randInt: (int) ->
    Math.floor Math.random() * int


  normalRand: ->
    (Math.random() * 2 - 1) +
    (Math.random() * 2 - 1) +
    (Math.random() * 2 - 1)


  interpolate: (start, end, step) ->
    return start if step <= 0
    return end if step >= 1
    start += (end - start) * step


  clamp: (value, min, max) ->
    Math.max min, Math.min value, max
