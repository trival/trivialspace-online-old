define [
  "external/threejs_extras/Detector"
], ->
  class BasicThreeJsApp

    scene: null
    renderer: null
    camera: null
    domElement: null
    modelLoader: null
    directRender: true
    paused: false
    fullscreen: false
    width: 800
    height: 600
    noWebGL: false
    mouseX: 0
    mouseY: 0
    trackMouse: false


    constructor: (configObject) ->

      $.extend @, configObject

      if typeof @domElement is 'string'
        @domElement = document.getElementById @domElement
      else unless @domElement
        @domElement = document.createElement "div"
        document.body.appendChild @domElement

      if @fullscreen
        @height = window.innerHeight
        @width = window.innerWidth
        document.body.style.overflow = 'hidden'
      else
        @domElement.style.width = @width + 'px'
        @domElement.style.height = @height + 'px'

      if not Detector.webgl
        @noWebGL = true
        Detector.addGetWebGLMessage parent: @domElement

      @modelLoader = new THREE.JSONLoader()
      load = (model, texture_path) =>
        jsonModel = $.parseJSON model
        geometry = null
        callback = (geo) ->
          geometry = geo

        @modelLoader.createModel jsonModel, callback, texture_path
        geometry

      @modelLoader.load = load


    init: ->


    update: ->


    start: ->
      unless @noWebGL
        @_internalInit()
        @init()
        @_animate()


    _animate: =>
      unless @paused
        requestAnimationFrame @_animate
        @_render()


    _internalInit: ->
      @scene ?= new THREE.Scene()
      @camera ?= new THREE.PerspectiveCamera 60, @width / @height, 0.01, 100
      @scene.add @camera
      @renderer ?= new THREE.WebGLRenderer antialias: true
      @renderer.setSize @width, @height
      @domElement.appendChild @renderer.domElement

      if @trackMouse
        $(@domElement).on 'mousemove', (evt) =>
          rect = @renderer.domElement.getBoundingClientRect()
          x = evt.clientX - rect.left
          y = evt.clientY - rect.top
          @mouseX = (x / @width) * 2 - 1
          @mouseY = (y / @height) * 2 - 1

      $(document).keypress (evt) =>
        key = evt.charCode
        if "p".charCodeAt(0) is key
          @paused = not @paused
          @_animate()  unless @paused

      if @fullscreen
        $(window).on "resize", @onResize
        $(document).on "keyup", (event) ->
          if event.keyCode == 27 #ESC
            top.history.back()

      else
        $(@domElement).on "resize", @onResize


    _render: ->
      @update()
      @renderer.render @scene, @camera  if @directRender


    onResize: (event) =>
      console.debug "resize event"
      @width = if @fullscreen then window.innerWidth else @domElement.innerWidth
      @height = if @fullscreen then window.innerHeight else @domElement.innerHeight
      console.debug "width: " + @width
      console.debug "height: " + @height
      @renderer.setSize @width, @height
      @camera.aspect = @width / @height
      @camera.updateProjectionMatrix()
