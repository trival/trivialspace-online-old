define [
  "text!./templates/PointLightController.html"
  "./widgetHelpers"

  "external/jscolor/jscolor"

  "ui/jquery-ui.min"

], (template, helpers) ->
  $.widget "trivialspace.pointlight",

    options:
      light: null
      scene: null
      scale: 1.0

    _create: ->
      helpers.loadWidgetCss()
      helpers.loadLightWidgetCss()

      light = @options.light
      pos = light.position

      @element.html template

      posX = @element.find('.light-position-x').spinner
        step: 0.5 * @options.scale
        spin: ->
          pos.x = posX.val()
        stop: ->
          pos.x = posX.val()
      posX.val pos.x

      posY = @element.find('.light-position-y').spinner
        step: 0.5 * @options.scale
        spin: ->
          pos.y = posY.val()
        stop: ->
          pos.y = posY.val()
      posY.val pos.y

      posZ = @element.find('.light-position-z').spinner
        step: 0.5 * @options.scale
        spin: ->
          pos.z = posZ.val()
        stop: ->
          pos.z = posZ.val()
      posZ.val pos.z

      lightMat = new THREE.MeshBasicMaterial(
        color: light.color.getHex()
        opacity: 0.6
        transparent: true
      )

      lightMesh = new THREE.Mesh(new THREE.SphereGeometry(0.2, 16, 8), lightMat)
      lightMesh.position = pos
      lightMesh.scale.x = lightMesh.scale.y = lightMesh.scale.z = @options.scale

      col = light.color
      colorInput = @element.find('.light-color')
      picker = new jscolor.color colorInput[0],
        pickerFaceColor: 'rgba(245,245,245,0.8)'
        pickerBorder: 0
        pickerFace: 5
      picker.fromRGB col.r, col.g, col.b
      colorInput.on "change", ->
        col.setRGB picker.rgb[0], picker.rgb[1], picker.rgb[2]
        lightMat.color = col

      visible = @element.find('.light-visible')
      visible.change =>
        if(visible.attr('checked'))
          @options.scene.add lightMesh
        else
          @options.scene.remove lightMesh

      enable = @element.find('.light-enable')
      enable.change =>
        if(enable.attr('checked'))
          @options.scene.add light
        else
          @options.scene.remove light


