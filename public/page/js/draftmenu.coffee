require [
  'page/pageUtils'
  'external/jquery-plugins/jquery.hoverIntent.minified'
], (pageUtils) ->

  isDraftMenuOpen = false
  content = null
  ul = null
  contentOffset = 0


  openMenu = ->
    unless isDraftMenuOpen
      isDraftMenuOpen = true
      contentPos = content.first().offset()
      contentOffset = 300 - contentPos.left
      ul.show "slow"

      if contentOffset > 0
        i = setInterval pageUtils.arrangeBottom, 100
        content.animate
          left: contentOffset + "px"
        , "slow", ->
          clearInterval i
          pageUtils.arrangeBottom()


  closeMenu = ->
    if isDraftMenuOpen
      isDraftMenuOpen = false
      ul.hide "slow"

      if contentOffset > 0
        i = setInterval pageUtils.arrangeBottom, 100
        content.animate
          left: "0"
        , "slow", ->
          clearInterval i
          pageUtils.arrangeBottom()


  toggleMenu = (event) ->
    event.preventDefault()
    if isDraftMenuOpen then closeMenu() else openMenu()


  initDraftMenu = ->
    menu = $ "#draftmenu"
    ul = menu.find "ul"
    content = $ ".content-container"

    $("#draftsButton").click toggleMenu

    ul.find('li').each ->
      $(this).click ->
        href = $(this).find('a').attr 'href'
        window.location.href = href

    menu.hoverIntent openMenu, closeMenu


  $ initDraftMenu
