define ->
  pageUtils =
    arrangeBottom : ->
      winHeight = $(window).height()
      pageHeight = $("#wrapper-inner").height()
      heightDiff = winHeight - pageHeight
      if heightDiff > 0
        $("#wrapper-bottom").css "bottom", -heightDiff
      else
        $("#wrapper-bottom").css "bottom", 0


  window.onresize = pageUtils.arrangeBottom
  pageUtils.arrangeBottom()

  $ ->
    pageUtils.arrangeBottom()
    setTimeout pageUtils.arrangeBottom, 100


  pageUtils
