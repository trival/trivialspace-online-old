require [
  'external/jquery-plugins/jquery.validate.min'
], ->

  resetForm = ->
    inputs = $ "#comment-form > .text > input, #comment-form textarea"
    console.debug inputs
    inputs.val ""

  $ ->
    form = $ "#comment-form"
    v = form.validate
      submitHandler: ->
        $.post '/newcomment', form.serialize(), success, 'html'

    success = (html) ->
      $("#comments-container").append html
      $("#comments-count").html $(".comment").length
      if $(".comment-error").length is 0
        resetForm()
        v.resetForm()

    resetForm()
    v.resetForm()

