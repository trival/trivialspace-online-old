define [
  "text!./templates/DirectionalLightController.html"
  "./widgetHelpers"

  "external/jscolor/jscolor"

  "ui/jquery-ui.min"

], (template, helpers) ->
  $.widget "trivialspace.dirlight",

    options:
      light: null
      scene: null
      scale: 1.0

    _create: ->
      helpers.loadWidgetCss()
      helpers.loadLightWidgetCss()

      light = @options.light
      light.position.normalize()
      pos = light.position.clone()
      rot = new THREE.Vector3()
      rotMat = new THREE.Matrix4()

      changeHandler = (spinner, dim) ->
        rot[dim] = spinner.val() * (Math.PI * 2) / 360
        rotMat.setRotationFromEuler rot
        light.position = rotMat.rotateAxis(pos.clone())
        displayDirection()

      @element.html template

      rotX = @element.find('.light-rotation-x').spinner
        step: 0.5
        min: 0.0
        max: 360.0
        spin: ->
          changeHandler(rotX,'x')
        stop: ->
          changeHandler(rotX,'x')
      rotX.val 0.0

      rotY = @element.find('.light-rotation-y').spinner
        step: 0.5
        min: 0.0
        max: 360.0
        spin: ->
          changeHandler(rotY,'y')
        stop: ->
          changeHandler(rotY,'y')
      rotY.val 0.0

      rotZ = @element.find('.light-rotation-z').spinner
        step: 0.5
        min: 0.0
        max: 360.0
        spin: ->
          changeHandler(rotZ,'z')
        stop: ->
          changeHandler(rotZ,'z')
      rotZ.val 0.0

      displayDirection = ->
        $('.light-direction-x').html light.position.x.toFixed(3)
        $('.light-direction-y').html light.position.y.toFixed(3)
        $('.light-direction-z').html light.position.z.toFixed(3)

      displayDirection()

      col = light.color
      colorInput = @element.find('.light-color')
      picker = new jscolor.color colorInput[0],
        pickerFaceColor: 'rgba(245,245,245,0.8)'
        pickerBorder: 0
        pickerFace: 5
      picker.fromRGB col.r, col.g, col.b
      colorInput.on "change", ->
        col.setRGB picker.rgb[0], picker.rgb[1], picker.rgb[2]

      enable = @element.find('.light-enable')
      enable.change =>
        if(enable.attr('checked'))
          @options.scene.add light
        else
          @options.scene.remove light


