nano = require 'nano'
config = require './server-config'
moment = require 'moment'

dbString = config.DB_URL + '/' + config.DB_DATABASE
db = nano dbString

title = "trivial space - "


# ---------------- helper methods ---------------------

renderDrafts = (res, draft, drafts) ->
  # db.view 'views/comments', {startkey: [draft._id], endkey: [draft._id, {}]}, (err, results) ->
    # if err or not results or results.length is 0
      # draft.comments = []
    # else
      # draft.comments = (result.value for result in results)
    res.render 'drafts',
      title: title + draft.name
      draft: draft
      drafts: drafts


getMenu = (next, callback) ->
  db.view 'views','menu', descending: true, (err, results) ->
    if err
      next err
    else if not results or results.length is 0
      next "no drafts found"
    else
      drafts = (result.value for result in results.rows)
      callback drafts


# --------------- route initialization -------------------

exports.init = (app) ->

  app.get '/about', (req, res, next) ->
    getMenu next, (drafts) ->
      res.render 'about',
        drafts: drafts


  app.get '/help', (req, res, next) ->
    getMenu next, (drafts) ->
      res.render 'help',
        drafts: drafts


  app.get '/disclaimer', (req, res, next) ->
    getMenu next, (drafts) ->
      res.render 'disclaimer',
        drafts: drafts


  app.param 'draftId', (req, res, next, draftId) ->
    db.get draftId, (err, doc) ->
      if err
        console.log 'draftId error: ', err
        next err
      else if not doc.active
        next "draft not found"
      else
        doc.time = moment doc.lastChange
        req.params.draft = doc
        next()


  app.get "/fullscreen/:draftId", (req, res, next) ->
    if req.params.draft
      res.render 'fullscreen',
            layout: false
            title: title + req.params.draft.name + ' (fullscreen)'
            draft: req.params.draft
    else next()


  app.get "/:draftId", (req, res, next) ->
    if req.params.draft
      getMenu next, (drafts) ->
        renderDrafts res, req.params.draft, drafts
    else next()


  app.get "/", (req, res, next) ->
    getMenu next, (drafts) ->
      db.get drafts[0].id, (err, draft) ->
        if err
          next "something went wront", err
        else
          renderDrafts res, draft, drafts


  # app.post "/newcomment", (req, res, next) ->
    # comment =
      # refId: req.body.refid
      # author: req.body.author
      # email: req.body.email
      # url: req.body.url
      # message: req.body.message
      # type: "comment"
      # active: true
      # timestamp: new Date().getTime()

    # db.save comment, (err, data) ->
      # res.type 'html'
      # if err
        # console.log err
        # res.render 'comment-error',
          # layout: false
      # else
        # res.render 'comment',
          # comment: comment
          # layout: false
